
/**
 *  @file Personalización y funcionamiento de un video
 *  Tarea de creación de un reproductor de video utilizando estándares web
 *  Asignatura: Streaming en la web
 * 
 *  @author Isaías Ismael Tello Lara
 *  @version 1.0.0
 *  
 */



/**
 *  Declaración de variables globales
 */
let vid = document.getElementById("videoplayer"),
    dev = true,
    currentVol = 0,
    volumenBar = document.getElementById('volume'),
    soundOnOff = document.getElementById('mute'),
    playOnOff = document.getElementById('playpause'),
    videoSpeed = document.getElementById('playbackrate'),
    cc = document.getElementById('ccbutton'),
    consolita = document.getElementById('mensajes'),
    progress = document.getElementById("progress"),
    thumb = document.querySelectorAll(".lista li")


/**
 * Registro de actividades
 * @param {string} data Cadena de texto para escribir en la ventana de estado (una especia de log) 
 * @return {void}
 */
function registro(data){
    if(dev) {
        consolita.textContent = data;
        consolita.classList.add('show');
        setTimeout(() => {
            consolita.classList.remove('show');
        }, 4000);
    }
}
registro('Comienza la magia del video');


/**
 * Acciones básicas
 */
vid.controls = false;
vid.textTracks[0].mode = "hidden";



/**
  *  Intervalo para actualizar el tiempo que ha transcurrido del video
  */
 var i = setInterval(function() {
	if(vid.readyState > 0) {
		var minutes = parseInt(vid.duration / 60, 10);
		var seconds = vid.duration % 60;
		clearInterval(i);
	} 
}, 200);


/**
 * Función para actulizar el tiempo de reproducción
 * @param {number} i Tiempo
 */
function seektimeupdate(){
    curtimetext = document.getElementById("curtimetext");
	durtimetext = document.getElementById("durtimetext");
	var nt = vid.currentTime * (100 / vid.duration);
    isNaN(nt) ? progress.value = 0 : progress.value = nt ;
    if(!isNaN(nt)){
        var curmins = Math.floor(vid.currentTime / 60);
        var cursecs = Math.floor(vid.currentTime - curmins * 60);
        var durmins = Math.floor(vid.duration / 60);
        var dursecs = Math.floor(vid.duration - durmins * 60);
        if(cursecs < 10){ cursecs = "0"+cursecs; }
        if(dursecs < 10){ dursecs = "0"+dursecs; }
        if(curmins < 10){ curmins = "0"+curmins; }
        if(durmins < 10){ durmins = "0"+durmins; }
        curtimetext.innerHTML = curmins+":"+cursecs;
        durtimetext.innerHTML = durmins+":"+dursecs;
    }
}

/**
 * Función para actualizar el porcentaje de reproducción en la barra de progreso al clic sobre ella
 * @param {*} e 
 */
 function seek(e) {
    let percent = e.offsetX / this.offsetWidth;
    vid.currentTime = percent * vid.duration;
    e.target.value = Math.floor(percent / 100);
    registro('Reproducción en la posición ' + Math.round(percent*100)+ '%');
    e.target.innerHTML = progress.value + '% reproducido';
}

/**
 * Función para actulizar el porcentaje de reproducción el barra de progreso
 */
function updateProgressBar() {
    var percentage = Math.floor((100 / vid.duration) * vid.currentTime);
    isNaN(percentage) ? percentage=0 : percentage=percentage ;
    progress.value = percentage;
    progress.innerHTML = percentage + '% reproducido';
}


/**
 * Función para disparar un evento
 * @param {element} el Elemento del dom
 * @param {event} etype Evento a disparar
 */
 function eventFire(el, etype){
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}


/**
 * Función para crear la imagen del fotograma
 */
 function imageCreate(){
    registro("Creando imagen…");
    const canvas = document.createElement("canvas");
    canvas.width = vid.videoWidth;
    canvas.height = vid.videoHeight;
    canvas.getContext('2d')
      .drawImage(vid, 0, 0, canvas.width, canvas.height);
    const dataURL = canvas.toDataURL();
    registro('Imagen en base64 '+ dataURL);
    var img = document.createElement("img");
    img.src = dataURL;
    document.getElementById('miniatura').src=dataURL;
    document.getElementById('dbutton').href=dataURL;
    document.getElementById('wimage').classList.add('show');

    gtag('event', 'Creando miniatura', {
        'event_category': 'Interacción con video',
        'event_label': 'El video: ' + document.querySelector("li.active").dataset.src,
        'value' : Math.round(vid.currentTime)
    });
}


/**
 * Función para moverse entre los diferentes videos disponibles
 * @param {*} x 
 */
 function moveOn(x){
    playOnOff.classList.remove("activated");
    cc.classList.remove('activated');
    vid.textTracks[0].mode = "hidden";


    //registro('Tenemos '+thumb.length+' películas disponibles');
    isma = 0;
    thumb.forEach(function(e,i){
        if (e.classList.contains('active'))  isma = i;
    });

    if(x==1) {
        //registro('Vamos a avanzar…');

        

        if((isma+1)==thumb.length) {
            //registro('Estamos en la última posición, veremos ahora ' + thumb[0]);
            eventFire(thumb[0], 'click');

        } else if (isma==0) {
            //registro('Estamos en la primer posición, veremos ahora ' + thumb[1]);
            eventFire(thumb[1], 'click');

        } else {
            //registro('Daremos click en ' + thumb[(isma+1)]);
            eventFire(thumb[(isma+1)], 'click');
        }

        gtag('event', 'Avanzar', {
            'event_category': 'Interacción con video',
            'event_label': 'Se reproducirá el video ' + document.querySelector("li.active").dataset.src
        });

    } else {
        //registro('Vamos a retroceder…');
        

        if (isma==0) {
            //registro('Estamos en la primer posición, veremos ahora ' + thumb[(thumb.length-1)]);
            eventFire(thumb[(thumb.length-1)], 'click');
        } else {
            //registro('Daremos click en, veremos ahora ' + thumb[(isma-1)]);
            eventFire(thumb[(isma-1)], 'click');
        }

        gtag('event', 'Retroceder', {
            'event_category': 'Interacción con video',
            'event_label': 'Se reproducirá el video ' + document.querySelector("li.active").dataset.src
        });


    }

    videoSpeed.value = 1;
}









/**
 * Evento cuando finaliza un video
 */
vid.addEventListener('ended',function(e){
    var autoPlay = 0.5;
    setTimeout(() => {
        eventFire(document.getElementById('nextButton'),'click');
        vid.play();
        playOnOff.classList.add('activated');
        playOnOff.textContent = "Reproducir";
    }, ((autoPlay+1)*1000));
},false);


/**
 * Evento cuando se actualiza el tiempo de reproducción
 */
vid.addEventListener("timeupdate",seektimeupdate,false);


/**
 * Evento para el botón pausar/reproducir
 */
playOnOff.addEventListener('click',function(){
    
    if(vid.paused) {
        registro('Reproduciendo video');
        playOnOff.textContent = "Pausar";
        playOnOff.classList.toggle("activated");
        vid.play();

        gtag('event', 'Reproducción', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    } else {
        registro('Video pausado');
        playOnOff.textContent = "Reproducir";
        playOnOff.classList.toggle("activated");
        vid.pause();

        gtag('event', 'Pausa', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    }
},false);


/**
 * Evento para activar o desactivar el sonido
 */
soundOnOff.addEventListener('click',function(){
    if(vid.volume != 0) {
        registro('Video silenciado');
        soundOnOff.textContent = 'Activar audio'
        currentVol = vid.volume;
        vid.volume = 0;
        soundOnOff.classList.remove('activated');

        gtag('event', 'Desactivar sonido', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    } else {
        registro('Sonido activado');
        soundOnOff.textContent = 'Silenciar'
        vid.volume = currentVol;
        soundOnOff.classList.add('activated');

        gtag('event', 'Activar sonido', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    }
    volumenBar.value = vid.volume;
},false);


/**
 * Evento para subir o bajar el volumen con el rango
 */
volumenBar.addEventListener('change',function(){
    (this.value > 0) ? soundOnOff.classList.add('activated') : soundOnOff.classList.remove('activated');
    registro('Volumen al '+(this.value*100)+'%');
    vid.volume = this.value;

    gtag('event', 'Cambiar volumen', {
        'event_category': 'Interacción con video',
        'event_label': 'El video: ' + document.querySelector("li.active").dataset.src,
        'value' : (this.value*100)
    });
},false);


/**
 * Evento para cambiar la velocidad de reproducción
 */
videoSpeed.addEventListener('change',function(){
    registro('Velocidad de reproducción '+ Math.round(this.value*100)+ '%');
    vid.playbackRate = this.value;

    gtag('event', 'Velocidad de reproducción', {
        'event_category': 'Interacción con video',
        'event_label': 'El video: ' + document.querySelector("li.active").dataset.src,
        'value' : Math.round(this.value*100)
    });
},false);


/**
 * Evento para cambiar el tamaño del video
 */
document.getElementById('size').addEventListener('change',function(){
    registro('Tamaño del video ' + (this.value*100) + '%');
    vid.style.height = (this.value*100)+'%';
    vid.style.width = (this.value*100)+'%';

    gtag('event', 'Tamaño del video', {
        'event_category': 'Interacción con video',
        'event_label': 'El video: ' + document.querySelector("li.active").dataset.src,
        'value' : Math.round(this.value*100)
    });
},false);


/**
 * Eventos para actualizar la barra de progreso al clic o durante la reproducción
 */
progress.addEventListener('click', seek);
vid.addEventListener('timeupdate', updateProgressBar, false);




/**
 * Evento para mover la película sobre la lista
 */
thumb.forEach(function (item, idx) {
    item.addEventListener('click', function(t){
        playOnOff.classList.remove('activated');
        playOnOff.textContent = 'Pausar';

        thumb.forEach(element => {
            element.classList.remove('active');
        });
        this.classList.add('active');
        registro('Vamos a ver el video: '+this.dataset.src);
        let path = "./assets/vid/",
            video = this.dataset.src,
            track = this.dataset.track,
            trackspath = "./assets/tracks/";

        sources = document.querySelectorAll('#videoplayer source');
        sources[0].src= path+video+'.mp4';
        sources[1].src= path+video+'.webm';
        progress.value = 0;
        trackv = document.querySelector("#videoplayer track");
        trackv.src= trackspath+track+'.vvt';

        vid.load();

        vid.textTracks[0].mode == "hidden";
        cc.classList.remove('activated');

        gtag('event', 'Selección de un video', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    }, true);
});


/**
 * Evento para activar o desactivar subtítulo
 */
cc.addEventListener('click', function(){
    
    if(vid.textTracks[0].mode == "showing") {
        registro('Ocultando subtítulos');
        vid.textTracks[0].mode = "hidden";
        cc.classList.remove('activated');

        gtag('event', 'Ocultando subtítulos', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    } else {
        registro('Mostrando subtítulos');
        vid.textTracks[0].mode = "showing";
        cc.classList.add('activated');

        gtag('event', 'Mostrando subtítulos', {
            'event_category': 'Interacción con video',
            'event_label': 'El video: ' + document.querySelector("li.active").dataset.src
        });
    }
});



